﻿2-4 players
Zidzidzidsebbeb
-by Sebastian Brox
The goal
Throw dice at your opponent until he gives up.
Contents
Each player must choose a colour:
* Red stars
* Green squares
* Yellow triangles
* Blue circles
The game contains:
* 1 playing board
* 12 dice
* 145 cards:
   * 100 movement cards:
      * "1": 15
      * "2": 25
      * "3": 20
      * "4": 25
      * " 5": 15
   * 45 weapon cards:
      * "9": 10
      * "11": 15
      * "12": 15
      * Shield: 5
* 5 pieces for each person
* 12 walls for each person
* Rules
Before the game starts
Movement and weapon cards are shuffled and put in different piles with the value of the card hidden. Dice and walls are placed in the bank. Each player places 5 pieces on a chosen movement field. The neutral wall is placed in the middle of the board.
On your turn:
1. Draw cards (necessary)
2. Movement cards
3. Attack
   1. Weapon cards
   2. Dice
   3. Walls
1. Draw cards
For each of your figurines you take a card, dice or wall. So, on your first turn you draw 5 movement cards, because there are 5 of your pieces on a movement field.
2. Movement cards 
You can use as many of your movement cards as you like. The sum of the numbers on the cards is the sum of the spaces you can move your pieces to. If you still haven't used all of your card total, you must use it after the attack action.
3.1. Weapon cards
Weapon cards are used to attack one of the opponent's figurines. The attacker and defender roll dice to get the highest number. Used dice are put in the bank.
3.1.1. "11"
With the card "11" you can attack pieces on the same field with 1 die.
3.1.2. "12"
With the card "12" you can attack pieces on the same field with 2 dice.
3.1.3 "9"
With the card "9" you can attack pieces 1 space from the piece or over a wall with 1 die.
3.1.4 The shield card
The defender can also use the shield card. This can also be done after rolling a die.
3.2. Dice
Dice are used to attack the opponent's pieces and destroy walls. How to attack is stated in the "weapon cards" section. To destroy a wall with dice, one of your pieces must be next to the wall you want to destroy. Then you roll one of your dice. If you get 4 or more, the wall goes to the bank. The die that was used also goes to the bank. 
The total dice you could have is limited to 12 divided by the number of players.
3.3. Walls
On your turn, you can place as many of your own walls as you want next to or under your pieces. How to destroy walls is stated in the "Dice" section. With the weapon card "9" you can shoot at a piece on the other side of the wall. Walls are owned by a player. The owner of the wall can walk over the wall as if there was nothing there, while all the other players cannot walk over.


The turn goes to the player to the left.


The last player with pieces on the board remaining wins.